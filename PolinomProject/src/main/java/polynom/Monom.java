package polynom;

public class Monom implements Comparable<Monom> {//implementeaza comparable pentru a putea face sortarea monoamelor
    private int coeficient;
    private int putere;
    private float coeficient2;

    public void setCoeficient(int coeficient) {
        this.coeficient = coeficient;
    }

    public float getRightCoeficient()//getter pentru coeficientul corespunzator adica cel intreg daca acela e diferit de 0 sau cel float daca acela e diferit de 0
    {
        if(this.getCoeficient()!=0)
            return this.getCoeficient();
        else
            return this.getCoeficient2();
    }
    public void setRightCoeficient(float a)//setter pentru coeficientul corespunzator adica cel intreg daca acela e diferit de 0 sau cel float daca acela e diferit de 0
    {
        if(this.getCoeficient()!=0)
            this.setCoeficient((int)a);
        else
            this.setCoeficient2(a);
    }
    public void setPutere(int putere) {
        this.putere = putere;
    }

    public Monom(int coeficient, int putere)
    {
        this.coeficient=coeficient;
        this.putere=putere;
    }

    public Monom(float coeficient, int putere)
    {
        this.coeficient2=coeficient;
        this.putere=putere;
    }
    public int getCoeficient() {
        return coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setCoeficient2(float coeficient2) {
        this.coeficient2 = coeficient2;
    }

    public float getCoeficient2() {
        return coeficient2;
    }

    @Override
    public int compareTo(Monom o) {//ordonarea descrescatoare a puterilor monomului
        return o.putere-this.putere;
    }
    public String toString()
    {
        if(this.getRightCoeficient()!=0)
            if(this.getRightCoeficient()>0)
                if(this.getRightCoeficient()%(int)getRightCoeficient()==0)//tratarea fiecarui caz de afisare in parte daca puterea e 0 sau daca e cu plus sau cu minus 
                    if(putere!=0)
                    return "+" + (int)this.getRightCoeficient() + "x^"+putere;
                    else
                        return "+" + (int)this.getRightCoeficient();
                else
                    if(putere!=0)
                        return "+" + this.getRightCoeficient() + "x^"+putere;
                    else
                        return "+" + this.getRightCoeficient();
            else
                if(this.getRightCoeficient()%(int)getRightCoeficient()==0)
                    if(putere!=0)
                        return (int)this.getRightCoeficient() + "x^"+putere;
                    else
                        return (int)this.getRightCoeficient()+"" ;
                else
                    if(putere!=0)
                        return  this.getRightCoeficient() + "x^"+putere;
                    else
                        return  this.getRightCoeficient() + "";


        else return "";
    }

}
