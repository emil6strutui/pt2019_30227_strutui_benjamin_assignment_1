package polynom;


import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {


    private ArrayList<Monom> monoame = new ArrayList<Monom>();
    private Character valider;
    public Polinom(String a) throws  Exception{

            createPolinom(a);


    }
    public Polinom(){}

    public void addMonom(Monom a) {
        this.monoame.add(a);
    }




    private void createPolinom(String a) throws Exception
    {
        a=this.cleanString(a); //metoda de curatare a stringului de caractere si spatii pentru o parsare mai usoara
        if(this.isValid(a)!=true) throw new Exception();//verificarea daca polinomul este alcatuit doar dintr-un singur coeficient
        Pattern pattern = Pattern.compile("([-+]?[^-+]+)");//stabilirea patternului
        Matcher matcher = pattern.matcher(a);
        String x;
        String [] split=null;

            while (matcher.find()) {//parcurgearea fiecarui matcher gasit
                x = matcher.group(0);
                if (x.contains(""+this.valider)) {//impartirea dupa caracterul de variabila
                    split = x.split(""+this.valider);
                    if (split.length == 2) {
                        if (isNumeric(split[0])) {
                            if (this.itContains(new Monom(Integer.parseInt(split[0]), Integer.parseInt(split[1]))) != true)//verificarea daca exista deja 
                                addMonom(new Monom(Integer.parseInt(split[0]), Integer.parseInt(split[1])));                //cumva monom cu aceasta putere
                        }
                        else {
                            if (this.itContains(new Monom(1, Integer.parseInt(split[1]))) != true)
                                addMonom(new Monom(1, Integer.parseInt(split[1])));
                        }
                    }

                    if (split.length == 1) {
                        if (isNumeric(split[0])) {
                            if (this.itContains(new Monom(Integer.parseInt(split[0]), 1)) != true)//Tratarea fiecarui caz in care poate fii monomum 2x x^2 fara x
                                addMonom(new Monom(Integer.parseInt(split[0]), 1));
                        }
                        else if (split[0].equals("+")) {
                            if (this.itContains(new Monom(1, 1)) != true)
                                addMonom(new Monom(1, 1));
                        }
                        else if (split[0].equals("-")) {
                            if (this.itContains(new Monom(-1, 1)) != true)
                                addMonom(new Monom(-1, 1));
                        }
                    }
                    if (split.length == 0) {
                        if(this.itContains(new Monom(1, 1))!=true)
                        addMonom(new Monom(1, 1));
                    }
                } else {
                    if (this.itContains(new Monom(Integer.parseInt(x), 0)) != true)
                        addMonom(new Monom(Integer.parseInt(x), 0));
                }
            }

            Collections.sort(monoame);//sortarea monomului dupa putere descresc

    }

    public void afisare()//afisarea polinomului in consola
    {
        for(Monom a:monoame)
        {
            System.out.print(a.toString());
        }
        System.out.println();
    }

    public void setMonoame(ArrayList<Monom> monoame) {//setarea unui alt array list la array listul monoame
        this.monoame = monoame;
    }

    public ArrayList<Monom> getMonoame() {
        return monoame;
    }

    private String cleanString(String a)//metoda de curatare a stringului de caractere si spatii pentru o parsare mai usoara
    {   a=a.replaceAll("\\s","");
        a=a.replaceAll("\\+-","\\-");
        a=a.replaceAll("\\-+","\\-");
        a=a.replaceAll("\\--","\\+");
        a=a.replaceAll("\\++","\\+");
        a=a.replaceAll("\\^","");
        return a;
    }
    private boolean isValid(String a)//verificarea daca polinomul este alcatuit doar dintr-un singur coeficient
    {
        valider='@';
        for(char x:a.toCharArray())
        {
           if(Character.isLetter(x))
           {
               if(valider.equals('@'))
                   valider=x;
               else if(valider.equals(x)!=true) return false;
           }
        }
        return true;
    }


    public String getMessage() {//construirea Stringului pentru afisarea in textfieldul din GUI
        String x="";
        if(monoame.size()>0)
            x=monoame.get(0).toString();

        if(x.contains("+"))
            x = x.replaceAll("\\+", "");

        for (Monom a : monoame)
        {
            if (a == monoame.get(0))
                continue;
            x = new String(x + a.toString());
        }

        return x;
    }
    private boolean itContains(Monom a)//daca exista deja monomul cu acea putere atunci sa adune doar coeficientul altfel false
    {
        for(Monom x:monoame)
            if (x.getPutere()==a.getPutere()) {
                x.setRightCoeficient(x.getRightCoeficient()+a.getRightCoeficient());
                return true;
            }
       return false;
    }
        public static boolean isNumeric(String str) {//verifica daca un string este numeric sau nu.
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
}
