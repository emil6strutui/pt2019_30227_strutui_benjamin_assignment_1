package polynom;

import java.util.Collections;

public interface Operations {


    static Polinom adunare(Polinom a, Polinom b)
    {

            int indx_a=0;
            while (indx_a < a.getMonoame().size())
            {
                for(Monom x:b.getMonoame())//adunarea coeficientilor de aceeasi putere in a
                    if (a.getMonoame().get(indx_a).getPutere() == x.getPutere()) {
                        a.getMonoame().get(indx_a).setCoeficient(a.getMonoame().get(indx_a).getCoeficient() + x.getCoeficient());
                        b.getMonoame().remove(x);//scoaterea celor adunate
                        break;
                    }

                indx_a++;
            }
            indx_a=0;
         while (indx_a < b.getMonoame().size())
         {
             for(Monom x:a.getMonoame())
                 if (b.getMonoame().get(indx_a).getPutere() != x.getPutere())//adaugarea monomilor ramasi din b in a
                 {
                     a.addMonom(new Monom(b.getMonoame().get(indx_a).getCoeficient(),b.getMonoame().get(indx_a).getPutere()));
                     break;
                 }
             indx_a++;

         }
         Collections.sort(a.getMonoame());//sortarea lui a si returnarea noului polinom
        return a;
    }
    static Polinom scadere(Polinom a, Polinom b)
    {
        int indx_a=0;
         while (indx_a < a.getMonoame().size()) {
             for(Monom x:b.getMonoame())
                 if (a.getMonoame().get(indx_a).getPutere() == x.getPutere()) {//scaderea coeficientilor de putere egală
                     a.getMonoame().get(indx_a).setRightCoeficient(a.getMonoame().get(indx_a).getRightCoeficient()-x.getRightCoeficient());
                     b.getMonoame().remove(x);
                     break;
                 }
             indx_a++;
         }
         indx_a=0;
         while (indx_a < b.getMonoame().size()) {
             for(Monom x:a.getMonoame())
                 if (b.getMonoame().get(indx_a).getPutere() != x.getPutere()) {//adaugarea monoamelor cu coeficientul cu minus din b in a
                     a.addMonom(new Monom(-(b.getMonoame().get(indx_a).getRightCoeficient()),b.getMonoame().get(indx_a).getPutere()));
                     break;
                 }
             indx_a++;
         }
         indx_a=0;
         while(indx_a!=a.getMonoame().size()) {
             Monom x=a.getMonoame().get(indx_a);//scoaterea monoamelor care au coeficient 0 posibil după scădere
             if (x.getCoeficient()==0 && x.getCoeficient2()==0)
                 a.getMonoame().remove(x);
             else indx_a++;
         }
         Collections.sort(a.getMonoame());//sortarea noului polinom și returnarea acestuia
        return a;
    }
     static Polinom derivare(Polinom a)
    {
        Polinom aux=new Polinom();
        int suma;
        int coeficient;
        int putere;
        for(Monom x:a.getMonoame())
        {
            coeficient=x.getCoeficient()*x.getPutere();//coeficientul se inmulteste cu puterea devenind noul coeficient iar din putere se scade 1
            putere=x.getPutere()-1;

            if(putere>=0 && coeficient!=0)
                aux.addMonom(new Monom(coeficient,putere));//adaugarea monomuluidacă coeficientul e diferit de 0
        }
        return aux;
    }
     public static Polinom integrare(Polinom a)
    {
        Polinom aux=new Polinom();
        int suma;
        float coeficient;
        int putere;
        for(Monom y:a.getMonoame())
        {

            coeficient=y.getCoeficient()/(float)(y.getPutere()+1);//impartirea coeficientul la float de putere+1 care formeaza nou coeficient
            putere=y.getPutere()+1;//adunarea la putere 1

            if(coeficient!=0)
                aux.addMonom(new Monom(coeficient,putere));
        }
        return aux;
    }
     static Polinom inmultire(Polinom a, Polinom b)
    {   Polinom aux=new Polinom();
        float coeficient;
        int putere;
        boolean found=false;
        for(Monom x:a.getMonoame())
            for(Monom y:b.getMonoame())
            {
                coeficient=x.getRightCoeficient()*y.getRightCoeficient();//se inmulteste fiecare monom cu fiecare monom in parte coeficient*coeficient 
                putere=x.getPutere()+y.getPutere();//iar puterile se aduna
                for(Monom z:aux.getMonoame())
                {
                    if(z.getPutere()==putere) {
                        z.setCoeficient2(z.getCoeficient2() + coeficient);//verificarea daca mai exista monom cu aceeasi putere ca si cea aflata
                        found=true;
                        break;
                    }
                }
                if(found==false)
                    aux.addMonom(new Monom(coeficient,putere));
                found=false;
            }
        Collections.sort(aux.getMonoame());//sortarea polinomului si returnarea acestuia
        return aux;
    }
    static Polinom[] impartire(Polinom a, Polinom b)
    {   Polinom[] p = new Polinom[2];
        float coeficient;
        int putere;
        Polinom aux=new Polinom();
        Polinom aux2=new Polinom();
        aux2.addMonom(new Monom(0,0));
        if(a.getMonoame().size()>0 && b.getMonoame().size()>0 && a.getMonoame().get(0).getRightCoeficient()==0)//daca primul ambele monoame is de size 0 sa se afiseze 0
        {
            p[0]=aux2;
            p[1]=aux2;
            return p;
        }
        while( a.getMonoame().size()>0 && b.getMonoame().size()>0 && a.getMonoame().get(0).getPutere()>=b.getMonoame().get(0).getPutere())
        {//pana cand cel ce trebuie impasrtit are o putere maxima mai mica decat cu cine impartim sa se execute algoritmul de impartire care se foloseste de
        	//scadere si inmultire
                  putere=a.getMonoame().get(0).getPutere()-b.getMonoame().get(0).getPutere();
                  coeficient=a.getMonoame().get(0).getRightCoeficient()/(float)b.getMonoame().get(0).getRightCoeficient();
                  aux.addMonom(new Monom(coeficient,putere));
                  aux2.getMonoame().get(0).setCoeficient2(coeficient);
                  aux2.getMonoame().get(0).setPutere(putere);
                  a=Operations.scadere(a,Operations.inmultire(aux2,b));
        }
        p[0]=aux;
        p[1]=a;
        return p;
    }

}


