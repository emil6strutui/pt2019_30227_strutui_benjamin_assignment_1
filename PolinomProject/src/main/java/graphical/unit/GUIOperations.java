package graphical.unit;

import polynom.Operations;
import polynom.Polinom;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIOperations extends Interfata {
     String TextField1;
     String TextField2;
     String Result;

    public GUIOperations(){
        super();
        this.butonAdunare();
        this.butonScadere();
        this.butonDerivare();
        this.butonIntegrare();
        this.butonInmultire();
        this.butonImpartire();
    }

    private void butonAdunare()
    {
        getAdunare().addActionListener(new ActionListener() { //adaugarea unui listener pe butonul de adunare
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom aux=new Polinom();
                TextField1=getField1().getText();  //Citirea celor 2 textfielduri
                TextField2=getField2().getText();
                try{
                    aux = Operations.adunare(new Polinom(TextField1), new Polinom(TextField2)); //incercarea crearii polinoamelor mai apoi apelarea operatiei
                    if (aux.getMessage().equals(""))
                        getField3().setText("0");
                    else {
                        Result = aux.getMessage();
                        getField3().setText(Result);
                    }
                }
                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                           //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }

            }
        });
    }
    private void butonScadere()
    {
        getScadere().addActionListener(new ActionListener() {//adaugarea unui listener pe butonul de scadere
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom aux = new Polinom();
                TextField1 = getField1().getText();
                TextField2 = getField2().getText();
                try {
                    aux = Operations.scadere(new Polinom(TextField1), new Polinom(TextField2));//incercarea crearii polinoamelor mai apoi apelarea operatiei
               
                    if (aux.getMessage().equals(""))
                        getField3().setText("0");
                    else {
                        Result = aux.getMessage();
                        getField3().setText(Result);
                    }
                }
                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                                 //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {  
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    private void butonDerivare ()
    {
        getDerivare().addActionListener(new ActionListener() {//adaugarea unui listener pe butonul de derivare
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom aux=new Polinom();
                TextField1=getField1().getText();
                try {
                    aux= Operations.derivare(new Polinom(TextField1)); //incercarea crearii polinomului mai apoi apelarea operatiei
                    if(aux.getMessage().equals(""))
                        getField3().setText("0");
                    else {
                        Result = aux.getMessage();
                        getField3().setText(Result);
                    }
                }
                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                         //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    private void butonIntegrare()
    {
        getIntegrare().addActionListener(new ActionListener() {//adaugarea unui listener pe butonul de integrare
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom aux=new Polinom();
                TextField1=getField1().getText();
                try{
                    aux= Operations.integrare(new Polinom(TextField1)); //incercarea crearii polinomului mai apoi apelarea operatiei
                    if(aux.getMessage().equals(""))
                        getField3().setText("0");
                    else {
                        Result = aux.getMessage();
                        getField3().setText(Result);
                        }
                    }
                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                    //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    private void butonInmultire()
    {
        getInmultire().addActionListener(new ActionListener() { //adaugarea unui listener pe butonul de inmultire
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom aux;
                TextField1=getField1().getText();
                TextField2=getField2().getText();
                try
                {
                    aux= Operations.inmultire(new Polinom(TextField1),new Polinom(TextField2)); //incercarea crearii polinoamelor mai apoi apelarea operatiei

                    if(aux.getMessage().equals(""))
                        getField3().setText("0"); //verificare daca s-a inmulit ceva si daca nu sa se afiseze 0
                    else {
                        Result = aux.getMessage();
                        getField3().setText(Result);
                    }
                }
                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                          //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    private void butonImpartire()
    {
        getImpartire().addActionListener(new ActionListener() { //adaugarea unui listener pe butonul de impartire
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom[] aux;
                TextField1=getField1().getText();
                TextField2=getField2().getText();
                try{
                    aux= Operations.impartire(new Polinom(TextField1),new Polinom(TextField2));//incercarea crearii polinoamelor mai apoi apelarea operatiei

                    if(aux[0].getMessage().equals("") && aux[1].getMessage().equals(""))
                        getField3().setText("0");
                    else if(!aux[0].getMessage().equals("") && aux[1].getMessage().equals("")) { //verificarea cazurilor dacă exista cat ,cat si rest 
                        Result = "catul:" +aux[0].getMessage()+ " restul:0";                      //sau doar rest si afisarea polinomului conform rezultatului
                        getField3().setText(Result);
                    }
                    else if(aux[0].getMessage().equals("") && !aux[1].getMessage().equals("")) {
                        Result = "catul:0" + " restul:"+aux[1].getMessage();
                        getField3().setText(Result);
                    }
                    else
                    {
                        Result = "catul:"+aux[0].getMessage() + " restul:"+aux[1].getMessage();
                        getField3().setText(Result);
                    }
                }

                catch (NumberFormatException exc) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }                                       //afisare de eroare in caz de polinom introdus greșit
                catch (Exception excep) {
                    JOptionPane.showMessageDialog(null,"Incorrect Polymons\nHint:3x^3+2x+1","Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}
