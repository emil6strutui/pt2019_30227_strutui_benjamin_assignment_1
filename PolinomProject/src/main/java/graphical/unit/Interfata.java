package graphical.unit;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

    public class Interfata {


        private JFrame frame = new JFrame();
        private JPanel panel1= new JPanel();
        private JPanel panel2= new JPanel();
        private JTextField field1= new JTextField("Introduce the first Polynom Here",50);
        private JTextField field2= new JTextField("Introduce the second Polynom Here",50);//formarea celor 2 JPanel uri unde sunt stocate butoanele si textfieldurile
        private JTextField field3 = new JTextField("Result",55);
        private JTextField field4= new JTextField("Polynom1:");
        private JTextField field5= new JTextField("Polynom2:");
        private JButton adunare = new JButton("Adunare");
        private JButton scadere = new JButton("Scadere");
        private JButton inmultire = new JButton("Inmultire");
        private JButton impartire = new JButton("Impartire");
        private JButton derivare = new JButton("Derivare P1");
        private JButton integrare = new JButton("Integrare P1");
        Interfata() {

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(600, 300);
            frame.setResizable(false);

            panel1.setLayout(new FlowLayout());
            panel2.setLayout(new FlowLayout());

            panel1.add(adunare);
            panel1.add(scadere);
            panel1.add(inmultire);
            panel1.add(impartire);
            panel1.add(derivare);
            panel1.add(integrare);

            panel2.add(field4);
            panel2.add(field1);
            panel2.add(field5);
            panel2.add(field2);
            panel2.add(field3);

            panel1.setBackground(Color.RED);
            panel2.setBackground(Color.BLUE);

            frame.add(panel1,BorderLayout.NORTH);
            frame.add(panel2,BorderLayout.CENTER);
            frame.setVisible(true);

            field4.setBackground(Color.ORANGE);
            field4.setEditable(false);
            field1.setBackground(Color.ORANGE);
            field2.setBackground(Color.ORANGE);
            field5.setEditable(false);
            field3.setEditable(false);
            field3.setBackground(Color.GRAY);
            field5.setBackground(Color.ORANGE);

        }
        public JButton getAdunare() {
            return adunare;
        }

        public JTextField getField1() {
            return field1;
        }

        public JTextField getField2() {
            return field2;
        }

        public JTextField getField3() {
            return field3;
        }

        public JButton getScadere() {
            return scadere;
        }

        public JButton getInmultire() {
            return inmultire;
        }

        public JButton getImpartire() {
            return impartire;
        }

        public JButton getDerivare() {
            return derivare;
        }

        public JButton getIntegrare() {
            return integrare;
        }
    }

