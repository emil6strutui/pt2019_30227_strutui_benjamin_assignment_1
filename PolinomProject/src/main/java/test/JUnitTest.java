package test;
import org.junit.Test;
import polynom.*;

import static org.junit.Assert.*;

public class JUnitTest  {
    @Test
    public void testAdunare1(){
        try {
            Polinom polinom1 = new Polinom("x^2+2x+5");
            Polinom polinom2 = new Polinom("2x^2+3x");
            assertEquals("3x^2+5x^1+5", Operations.adunare(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testAdunare2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            assertNotEquals("3x^3+2x+3", Operations.adunare(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testScadere1(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            assertEquals("-3x^2+2x^1+1", Operations.scadere(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testScadere2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            assertNotEquals("3x^2+2x^1+3", Operations.scadere(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testInmultire1(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            assertEquals("6x^3+6x^2+2x^1+2", Operations.inmultire(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testInmultire2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            assertNotEquals("", Operations.adunare(polinom1, polinom2).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testImpartire1(){
        try {
            Polinom polinom1 = new Polinom("2x^2+2x");
            Polinom polinom2 = new Polinom("x");
            Polinom[] aux=Operations.impartire(polinom1, polinom2);
            assertEquals("catul:2x^1+2 restul:0", "catul:"+aux[0].getMessage()+" restul:0");
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testImpartire2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            Polinom polinom2 = new Polinom("3x^2+1");
            Polinom[] aux=Operations.impartire(polinom1, polinom2);
            assertNotEquals("catul:1 restul 2x^1+2","catul:"+aux[0].getMessage()+" restul:"+aux[1].getMessage() );
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testDerivare1(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            assertEquals("2", Operations.derivare(polinom1).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testDerivare2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            assertNotEquals("2x+2", Operations.derivare(polinom1).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testIntegrare1(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            assertNotEquals("x^2+2x", Operations.integrare(polinom1).getMessage());
        }
        catch(Exception e)
        {

        }
    }
    @Test
    public void testIntegrare2(){
        try {
            Polinom polinom1 = new Polinom("2x+2");
            assertNotEquals("2x^2+2x", Operations.integrare(polinom1).getMessage());
        }
        catch(Exception e)
        {

        }
    }
}
